<div class="row removable">
    <div class="col-md-2"></div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="name">@lang('Menu Name')</label>
            <input type="text" class="form-control" id="name{{ $index }}"
                   name="menu_name[{{ $index }}]" placeholder="@lang('Please input')">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="price">@lang('Menu Price')</label>
            <input type="number" class="form-control" id="price{{ $index }}"
                   name="menu_price[{{ $index }}]" placeholder="@lang('Please input')">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="category">@lang('Menu Category')</label>
            <select class="form-control input-solid" id="category{{ $index }}" name="category_id[{{ $index }}]">
                <option value=""></option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-1 pt-4">
        <label class="btnRmv">
            <span class="info-2">
                <i class="fas fa-minus"></i> Remove
            </span>
        </label>
    </div>
</div>

<script>
    $(function(){
        bsCustomFileInput.init();
        
        $("#category{{ $index }}").select2({"allowClear":true,"placeholder":{"id":"","text":"Please Select Option"}});

        $(".btnRmv").on("click", function(){
            $(this).closest('div.removable').remove();
        });
    })
</script>