<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class TTransactions extends Model
{
    protected $table = 't_transactions';

    protected $fillable = [
        'user_id',
        'order_id',
        'total_payment',
        'payment_type',
        'payment_completed_at'
    ];

    protected $guarded = [];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(TOrders::class);
    }
}