<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class TOrdersDetail extends Model
{
    protected $table = 't_orders_detail';

    protected $fillable = [
        'order_id',
        'menu_id',
        'qty',
        'total_price',
        'notes'
    ];

    protected $guarded = [];

    public function menu()
    {
        return $this->belongsTo(TRestaurantMenus::class, 'menu_id');
    }
}