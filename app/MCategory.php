<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class MCategory extends Model
{
    protected $table = 'm_category';

    protected $guarded = [];
}
