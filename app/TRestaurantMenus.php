<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class TRestaurantMenus extends Model
{
    protected $table = 't_restaurants_menus';

    protected $fillable = [
        'restaurant_id',
        'category_id',
        'menu_name',
        'menu_price'
    ];

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(MCategory::class);
    }
}