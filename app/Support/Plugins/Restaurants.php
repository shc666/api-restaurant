<?php

namespace Vanguard\Support\Plugins;

use Vanguard\Plugins\Plugin;
use Vanguard\Support\Sidebar\Item;

class Restaurants extends Plugin
{
    public function sidebar()
    {
        return Item::create(__('Restaurants'))
            ->route('restaurants.index')
            ->icon('fas fa-utensils')
            ->active("restaurants*")
            ->permissions('restaurant.manage');
    }
}