<?php

namespace Vanguard\Http\Controllers\Web;

use Vanguard\Http\Requests\Restaurant\CreateUpdateRequest;
use Vanguard\Http\Controllers\Controller;
use Vanguard\MCategory;
use Vanguard\TRestaurant;
use Vanguard\TRestaurantMenus; 
use DataTables;

class ResturantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:restaurant.manage');
    }

    public function getDataRestaurants()
    {
        $restaurants = TRestaurant::latest()->get();
            
        return DataTables::of($restaurants)
        ->addIndexColumn()
        ->addColumn('action', function($restaurants) {                
            $edit = '
            <a data-toggle="tooltip" title="Edit Data" href="'.route('restaurants.edit',['restaurant' => $restaurants->id]).'" class="btn btn-outline-info btn-sm"><i class="fas fa-edit"></i></a>
            <a data-toggle="tooltip" data-placement="top" data-method="DELETE" data-confirm-title="Confirm" data-confirm-text="Are you sure to delete this data?" data-confirm-delete="Delete" title="Delete" href="'.route('restaurants.destroy',['restaurant' => $restaurants->id]).'" class="btn btn-outline-danger btn-sm"><i class="fas fa-trash"></i></a>
            ';
            return $edit;
        })
        ->rawColumns(['action'])
        ->toJson();
    }

    public function addMenus($index)
    {
        $categories = MCategory::get(['id', 'name']);

        return view('ajax.menus', compact('categories', 'index'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('restaurants.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restaurants.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUpdateRequest $request)
    {
        $inputs = $request->all();

        $restaurant = TRestaurant::create($inputs);

        $menus = $inputs['menu_name'];
        if($menus)
        {
            foreach($menus as $k => $menu)
            {
                $menu_res = new TRestaurantMenus;
                $menu_res->restaurant_id = $restaurant->id;
                $menu_res->category_id   = $inputs['category_id'][$k];
                $menu_res->menu_name     = $inputs['menu_name'][$k];
                $menu_res->menu_price    = $inputs['menu_price'][$k];
                $menu_res->save();
            }
        }

        return redirect()->route('restaurants.index')
            ->withSuccess('Success submited data');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restaurant = TRestaurant::find($id);
        $categories = MCategory::get(['id', 'name']);
        $menus = TRestaurantMenus::whereRestaurantId($id)->get();

        return view('restaurants.edit', compact('categories', 'menus', 'restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateUpdateRequest $request, $id)
    {
        $inputs = $request->all();
        
        $restaurant = TRestaurant::find($id);
        $restaurant->update($inputs);

        $menus = $inputs['menu_name'];
        if(!is_null($menus))
        {
            TRestaurantMenus::where('restaurant_id', $id)->delete();
            foreach($menus as $k => $menu)
            {
                $menu_res = new TRestaurantMenus;
                $menu_res->restaurant_id = $restaurant->id;
                $menu_res->category_id   = $inputs['category_id'][$k];
                $menu_res->menu_name     = $inputs['menu_name'][$k];
                $menu_res->menu_price    = $inputs['menu_price'][$k];
                $menu_res->save();
            }
        }

        return redirect()->back()
            ->withSuccess('Success updated data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restaurant = TRestaurant::findOrFail($id);
        $restaurant_menus = TRestaurantMenus::where('restaurant_id', $restaurant->id);
        $restaurant_menus->delete();
        $restaurant->delete();

        return redirect()->back()
            ->withSuccess('Success updated data');
    }
}