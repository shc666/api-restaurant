<?php

namespace Vanguard\Http\Controllers\Api;

use Vanguard\Http\Requests\Orders\CreateOrdersRequest;
use Vanguard\Http\Requests\Transactions\PaymentRequest;
use Vanguard\Http\Requests\Transactions\ListTransactions;
use Vanguard\Http\Requests\Transactions\ListTransactionRange;
use Vanguard\TOrders;
use Vanguard\TOrdersDetail;
use Vanguard\TTransactions;
use Vanguard\Http\Resources\OrderResource;
use Vanguard\Http\Resources\SuccessOrderResource;
use Vanguard\Http\Resources\TransactionListResource;
use Vanguard\Http\Resources\TransactionListRestaurantResource;
use Vanguard\Http\Resources\TransactionListUserResource;
use Vanguard\Http\Resources\ListTransactionsRangeResource;
use Vanguard\Http\Resources\TransactionResource;
use DB;

class TransactionsOrdersController extends ApiController
{

    /**
     * Type List
     */
    public function getTypeList($type)
    {
        $arrays = [
            1   => "Restaurant",
            2   => "User",
            3   => "Popular"
        ];

        foreach($arrays as $key => $array)
        {
            if($key == $type)
            {
                return $array;
            }
        }
        
        return abort(422);
    }

    /**
     * Get the Order
     */ 
    public function getOrderDetail($value)
    {
        $order = TOrders::find($value);
        if($order->status == 0)
        {
            $order->status = 'Processing';
        }
        elseif($order->status == 1)
        {
            $order->status = 'Success';
        }
        else
        {
            $order->status = 'Cancel';
        }

        return new OrderResource($order);
    }

    /**
     * Store the Order
     */
    public function store(CreateOrdersRequest $request)
    {
        $inputs = $request->only(['user_id', 'menu_id', 'notes', 'restaurant_id', 'total_price', 'qty']);
        $menus = $inputs['menu_id'];
        $total_price = array_sum($inputs['total_price']);
        
        // $orders = TOrders::create($inputs);

        $orders = new TOrders;
        $orders->user_id        = $inputs['user_id'];
        $orders->restaurant_id  = $inputs['restaurant_id'];
        $orders->total_payment  = $total_price;
        $orders->status         = 0;
        $orders->save();

        if($menus)
        {
            foreach($menus as $k => $menu)
            {
                $detail = new TOrdersDetail;
                $detail->order_id       = $orders->id;
                $detail->menu_id        = $inputs['menu_id'][$k];
                $detail->qty            = $inputs['qty'][$k];
                $detail->total_price    = $inputs['total_price'][$k];
                $detail->notes          = $inputs['notes'][$k];
                $detail->save();
            }
        }

        return new SuccessOrderResource($orders);
    }

    /**
     *  Make a transactions 
     */
    public function transactionsPayment(PaymentRequest $request)
    {
        $inputs = $request->only(['order_id']);

        $order = $this->getOrderDetail($inputs['order_id']);

        $transaction = new TTransactions;
        $transaction->user_id              = $order->user_id;
        $transaction->order_id             = $order->id;
        $transaction->total_payment        = $order->total_payment;
        $transaction->payment_completed_at = now();
        $transaction->save();

        $change_status = TOrders::find($transaction->order_id);
        $change_status->status = 1;
        $change_status->save();

        return new TransactionResource($transaction);
    }

    /**
     * List Transactions
     */
    public function listTransactions(ListTransactions $request)
    {
        $inputs = $request->only(['list', 'restaurant_id', 'user_id']);
        $list = $inputs['list'];

        $type = $this->getTypeList($list);
        
        if($list == 1)
        {
            $transaction = TTransactions::whereHas('order.restaurant', function($query) use($inputs) {
                            $query->where('restaurant_id', $inputs['restaurant_id']);
                        })
                        ->get();

            return TransactionListRestaurantResource::collection($transaction);
        }
        elseif($list == 2)
        {
            $transaction = TTransactions::whereHas('user', function($query) use($inputs) {
                            $query->where('user_id', $inputs['user_id']);
                        })
                        ->get();

            return TransactionListUserResource::collection($transaction);
        }
        elseif($list == 3)
        {
            $transaction = TTransactions::select(
                                't_transactions.total_payment', 
                                'tr.name',
                                DB::raw('count(tr.name) as sum_name')
                            )
                            ->join('t_orders as tor', 'tor.id', '=', 't_transactions.order_id')
                            ->join('t_restaurants as tr', 'tr.id', '=', 'tor.restaurant_id')
                            ->groupBy('t_transactions.total_payment', 'tr.name')
                            ->orderBy('t_transactions.total_payment', 'desc')
                            ->get();

            return TransactionListResource::collection($transaction);
        }
    }

    /**
     * List Transactions By User with range
     */

     public function listTransactionRange(ListTransactionRange $request)
     {
        $inputs = $request->only(['range_top', 'range_bottom']);
        $top = $inputs['range_top'];
        $bottom = $inputs['range_bottom'];

        $transaction = TTransactions::join('users as u', 'u.id', '=', 't_transactions.user_id')
                        ->whereBetween('payment_completed_at', [$bottom, $top])
                        ->select(
                            'u.id', 
                            'u.first_name', 
                            'u.last_name',
                            DB::raw('sum(t_transactions.total_payment) as total_payment_all')
                        )
                        ->groupBy('u.id', 'u.first_name', 'u.last_name')
                        ->orderBy('total_payment_all', 'desc')
                        ->get();

        return ListTransactionsRangeResource::collection($transaction);
     }
}