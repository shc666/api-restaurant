<?php

namespace Vanguard\Http\Controllers\Api;

use Vanguard\Http\Requests\Restaurant\ListCertainRequest;
use Vanguard\Http\Requests\Restaurant\ListLocationRequest;
use Vanguard\Http\Requests\Restaurant\ListDishNumberRequest;
use Vanguard\Http\Requests\Restaurant\SearchDishMatchRequest;
use Vanguard\Http\Resources\RestaurantResource;
use Vanguard\Http\Resources\ListRestaurantDishRangeResource;
use Vanguard\Http\Resources\ListRestaurantLocationResource;
use Vanguard\TRestaurant;
// use Stevebauman\Location\Facades\Location;
use DB;

class RestaurantController extends ApiController
{
    /**
     * List all restaurant
     */
    public function index()
    {
        $data = TRestaurant::with(['menus:id,restaurant_id,menu_name,menu_price'])->get();

        $restaurant = [];

        foreach($data as $key => $value)
        {
            /* Status */
            if($value->status == 1)
            {
                $value->status = 'Active';
            }
            else
            {
                $value->status = 'Non Active';
            }

            /* Open Fullday */
            if($value->open_fullday == 1)
            {
                $value->open_fullday = 'Open Fullday';
            }
            else
            {
                $value->open_fullday = 'Non Open Fullday';
            }

            $restaurant[] = $value;
        }

        return RestaurantResource::collection($restaurant);
    }

    /**
     * List Restaurant Open Certain Time
     */
    public function listOpen(ListCertainRequest $request)
    {
        $inputs = $request->only(['allday']);
        
        if(!empty($inputs['allday']) && $inputs['allday'] == 1)
        {
            $restaurant = TRestaurant::with(['menus:id,restaurant_id,menu_name,menu_price'])
                            ->where('open_fullday', 1)
                            ->get();
        }
        else
        {
            $restaurant = TRestaurant::with(['menus:id,restaurant_id,menu_name,menu_price'])->get();
        }

        return RestaurantResource::collection($restaurant);
    }

    /**
     * List Restaurant With Location
     */
    public function listLocation(ListLocationRequest $request)
    {
        $inputs = $request->only(['latitude', 'longitude']);
        $lat = $inputs['latitude'];
        $long = $inputs['longitude'];

        // Google Map Encode Geocode
        /* $location = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($long).'&sensor=false&key=AIzaSyDdbAiM3O6-SrlTyQEYGk3wXVFCw-2ubPs');
        $location_detail = json_decode($location);
        $address = $location_detail->address_components['formatted_address'];
        dd($location_detail->results[0]->formatted_address); */

        $data = TRestaurant::select(
                    "t_restaurants.*",
                    DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                    * cos(radians(t_restaurants.map_lat)) 
                    * cos(radians(t_restaurants.map_long) - radians(" . $long . ")) 
                    + sin(radians(" .$lat. ")) 
                    * sin(radians(t_restaurants.map_lat))) AS distance"))
                ->groupBy("t_restaurants.id")
                ->orderBy('distance', 'asc')
                ->get();

        $restaurant = [];

        foreach($data as $key => $value)
        {
            /* Status */
            if($value->status == 1)
            {
                $value->status = 'Active';
            }
            else
            {
                $value->status = 'Non Active';
            }

            /* Open Fullday */
            if($value->open_fullday == 1)
            {
                $value->open_fullday = 'Open Fullday';
            }
            else
            {
                $value->open_fullday = 'Non Open Fullday';
            }

            $restaurant[] = $value;
        }

        return ListRestaurantLocationResource::collection($restaurant);
    }

    /**
     * List Restaurant With Dishes Price Range
     */
    public function listDishNumber(ListDishNumberRequest $request)
    {
        $inputs = $request->only(['range_top', 'range_bottom']);
        $top = $inputs['range_top'];
        $bottom = $inputs['range_bottom'];

        // $restaurant = TRestaurant::with(['menus:id,restaurant_id,menu_name,menu_price'])
        //             ->whereHas('menus', function($query) use($bottom,$top) {
        //                 $query->whereBetween('menu_price', [$bottom, $top]);
        //             })
        //             ->get();

        $restaurant = TRestaurant::join('t_restaurants_menus as trm', 'trm.restaurant_id', '=', 't_restaurants.id')
                    ->whereBetween('trm.menu_price', [$bottom, $top])
                    ->select('t_restaurants.*', 'trm.menu_name', 'trm.menu_price')
                    ->groupBy('t_restaurants.id', 'trm.menu_name', 'trm.menu_price')
                    ->get();
        
        return ListRestaurantDishRangeResource::collection($restaurant);
    }

    /**
     * Search that has a dish matching search term
     */
    public function searchDishMatch(SearchDishMatchRequest $request)
    {
        $inputs = $request->only(['query_search']);
        $search_keywords = $inputs['query_search'];
        if($search_keywords)
        {
            $restaurant = TRestaurant::SearchByDishKeyword($search_keywords)->get();
        }

        return RestaurantResource::collection($restaurant);
    }
}