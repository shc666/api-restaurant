<?php

namespace Vanguard\Http\Requests\Restaurant;

use Vanguard\Http\Requests\Request;

class ListCertainRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}