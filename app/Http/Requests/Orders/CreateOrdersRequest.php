<?php

namespace Vanguard\Http\Requests\Orders;

use Vanguard\Http\Requests\Request;

class CreateOrdersRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => 'required',
            'menu_id'       => 'required',
            'qty'           => 'required',
            'total_price'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'       => 'User must be filled',
            'menu_id.required'       => 'Menu must be filled',
            'qty.required'           => 'Qty must be filled',
            'total_price.required'   => 'Total price menu must be filled'
        ];
    }
}
