<?php

namespace Vanguard\Http\Requests\Transactions;

use Vanguard\Http\Requests\Request;

class PaymentRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'      => 'required',
            'payment_type'  => 'required'
        ];
    }

    public function messages()
    {
        return [
            'order_id.required'     => 'Order ID must be filled',
            'payment_type.required' => 'Payment type must be filled'
        ];
    }
}