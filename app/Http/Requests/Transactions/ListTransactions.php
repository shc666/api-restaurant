<?php

namespace Vanguard\Http\Requests\Transactions;

use Vanguard\Http\Requests\Request;

class ListTransactions extends Request
{
   /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'list' => 'required'
        ];
    }
}