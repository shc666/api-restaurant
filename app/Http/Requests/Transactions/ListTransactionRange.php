<?php

namespace Vanguard\Http\Requests\Transactions;

use Vanguard\Http\Requests\Request;

class ListTransactionRange extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'range_top'     => 'required|date_format:Y-m-d',
            'range_bottom'  => 'required|date_format:Y-m-d'
        ];
    }

    public function messages()
    {
        return [
            'range_top.required'       => 'Range top value must be filled',
            'range_top.date_format'    => 'Range top value must be with format Y-m-d',
            'range_bottom.required'    => 'Range bottom value must be filled',
            'range_bottom.date_format' => 'Range bottom value must be with format Y-m-d',
        ];
    }
}
