<?php

namespace Vanguard\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionListRestaurantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'              => $this->order_id,
            'restaurant'            => [
                'id'                => $this->order->restaurant->id,
                'name'              => $this->order->restaurant->name
            ],
            'total_transactions'    => $this->total_payment,
            'payment_completed_at'  => $this->payment_completed_at
        ];
    }
}
