<?php

namespace Vanguard\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'restaurant_name'           => $this->name,
            'restaurant_address'        => $this->address,
            'restaurant_map_lat'        => floatval($this->map_lat),
            'restaurant_map_long'       => floatval($this->map_long),
            'restaurant_cash_balance'   => $this->cash_balance,
            'restaurant_open_monday'    => $this->working_hours_mon,
            'restaurant_open_tuesday'   => $this->working_hours_tue,
            'restaurant_open_wednesday' => $this->working_hours_wed,
            'restaurant_open_thursday'  => $this->working_hours_thurs,
            'restaurant_open_friday'    => $this->working_hours_fri,
            'restaurant_open_saturday'  => $this->working_hours_sat,
            'restaurant_open_sunday'    => $this->working_hours_sun,
            'restaurant_status'         => $this->status,
            'restaurant_openfullday'    => $this->open_fullday,
            'restaurant_menu'           => $this->menus
        ];
    }
}