<?php

namespace Vanguard\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionListUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'              => $this->order_id,
            'user'                  => [
                'id'                => $this->user->id,
                'fullname'          => $this->user->first_name.' '.$this->user->last_name
            ],
            'total_payment'         => $this->total_payment,
            'payment_completed_at'  => $this->payment_completed_at
        ];
    }
}
