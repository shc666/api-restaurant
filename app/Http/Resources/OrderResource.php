<?php

namespace Vanguard\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id'      => $this->id,
            'user'          => [
                'id'        => $this->user->id,
                'fullname'  => $this->user->first_name.' '.$this->user->last_name
            ],
            'restaurant'    => [
                'id'        => $this->restaurant->id,
                'name'      => $this->restaurant->name
            ],
            'status'        => $this->status,
            'total_payment' => $this->total_payment
        ];
    }
}