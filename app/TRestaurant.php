<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class TRestaurant extends Model
{
    protected $table = 't_restaurants';

    protected $fillable = [
        'name',
        'address',
        'map_lat',
        'map_long',
        'cash_balance',
        'working_hours_mon',
        'working_hours_tue',
        'working_hours_wed',
        'working_hours_thurs',
        'working_hours_fri',
        'working_hours_sat',
        'working_hours_sun',
        'open_fullday',
        'status'
    ];

    protected $guarded = [];

    public function menus()
    {
        return $this->hasMany(TRestaurantMenus::class, 'restaurant_id');
    }

    public function scopeSearchByDishKeyword($query, $search_keywords)
    {
        $query->whereHas('menus', function($query) use ($search_keywords) {
            $query->where('menu_name', 'LIKE', "%$search_keywords%");
        });

        return $query;
    }
}