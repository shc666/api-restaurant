<?php

namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class TOrders extends Model
{
    protected $table = 't_orders';

    protected $fillable = [
        'user_id',
        'restaurant_id',
        'total_payment',
        'status'
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function restaurant()
    {
        return $this->belongsTo(TRestaurant::class);
    }
}