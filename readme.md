## Vanguard - Advanced PHP Login and User Management

- Developed by [Chandrayana Putra]

# How to use API
- Download or clone this repo
- After finish clone this repo, use syntax "composer install" in terminal
- Copy and paste .env.example. And rename it to ".env"
- In .env file, matching to your database environment. Start from "DB_DATABASE, DB_USERNAME, DB_PASSWORD" 
- Import raw database in folder "Database" with filename "Raw_database.sql" to your database
- Run the API with syntax "php artisan serve"

# Backend Access
- This backend feature also can use it to input the data as well.
- The link for backend : http:localhost:8000/login
- Use this credentials for admin role:
- username: admin
- password: admin123

# API Documentation
- You can read the API Documentation with this URL: https://documenter.getpostman.com/view/5936761/UV5RoM4A